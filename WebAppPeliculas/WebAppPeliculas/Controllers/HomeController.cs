﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebAppPeliculas.Models;

namespace WebAppPeliculas.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        private string usuario = "";
        private string tokenAccess = "";

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

    

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult Gestionar()
        {
            if (Validacion() == false)
            {
                return RedirectToAction("Login", "Usuario");
            }
            else
            {
                return View();
            }
        }

        public bool Validacion()
        {
            try
            {
                usuario = Request.Cookies["USUARIOWAP"];
                tokenAccess = Request.Cookies["ACCESS_TOKEN_PELIS"];
                if (usuario == null || usuario == "" || tokenAccess == null || tokenAccess == "")
                {
                    return false;
                }

                else
                {
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }



        }

    }
}
