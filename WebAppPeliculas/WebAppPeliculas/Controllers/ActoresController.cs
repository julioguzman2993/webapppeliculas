﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebAppPeliculas.Models.Entity;
using WebAppPeliculas.Servicies;

namespace WebAppPeliculas.Controllers
{
    public class ActoresController : Controller
    {
        private string usuario = "";
        private string tokenAccess = "";
        ActualHost actualHost = new ActualHost();

        public IActionResult Index()
        {
            IEnumerable<ActorEntity> lstActor = null;
            try
            {

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(actualHost.HostNameActual + "Actores/ListadoActoresPublico");
                    var responseTask = client.GetAsync("ListadoActoresPublico");
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readJob = result.Content.ReadAsAsync<IList<ActorEntity>>();
                        readJob.Wait();
                        lstActor = readJob.Result;
                    }
                    else
                    {
                        lstActor = Enumerable.Empty<ActorEntity>();

                    }


                }

                return View(lstActor);
            }
            catch (Exception)
            {

                lstActor = Enumerable.Empty<ActorEntity>();
                return View(lstActor);
            }
        }

        public IActionResult ListadoActores()
        {
            IEnumerable<ActorEntity> lstActor = null;
            try
            {
                if (Validacion() == false)
                {
                    return RedirectToAction("Login", "Usuario");
                }

                using (var client = new HttpClient())
                {
                    string cookieToken = Request.Cookies["ACCESS_TOKEN_PELIS"];
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", cookieToken);
                    client.BaseAddress = new Uri(actualHost.HostNameActual + "Actores/ListadoActores");
                    var responseTask = client.GetAsync("ListadoActores");
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readJob = result.Content.ReadAsAsync<IList<ActorEntity>>();
                        readJob.Wait();
                        lstActor = readJob.Result;
                    }
                    else
                    {
                        lstActor = Enumerable.Empty<ActorEntity>();

                    }


                }

                return View(lstActor);
            }
            catch (Exception)
            {

                lstActor = Enumerable.Empty<ActorEntity>();
                return View(lstActor);
            }


        }

        [HttpGet]
        public IActionResult GuardarActor()
        {
            if (Validacion() == false)
            {
                return RedirectToAction("Login", "Usuario");
            }
            return View();

        }

        [HttpPost]
        public IActionResult GuardarActor(ActorEntity actor)
        {
            try
            {
                if (Validacion() == false)
                {
                    return RedirectToAction("Login", "Usuario");
                }
                using (var client = new HttpClient())
                {
                    string cookieToken = Request.Cookies["ACCESS_TOKEN_PELIS"];
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", cookieToken);
                    client.BaseAddress = new Uri(actualHost.HostNameActual + "Actores/GuardarActor");
                    var postJob = client.PostAsJsonAsync<ActorEntity>("GuardarActor", actor);
                    postJob.Wait();

                    var postResult = postJob.Result;
                    if (postResult.IsSuccessStatusCode)
                    {
                        return RedirectToAction("ListadoActores");
                    }
                    else
                    {
                        return View();
                    }

                }

            }
            catch (Exception)
            {

                return View();
            }


        }

        [HttpGet]
        public IActionResult EditarActor(int id)
        {
            try
            {
                if (Validacion() == false)
                {
                    return RedirectToAction("Login", "Usuario");
                }
                if (id.ToString() == "" || id.ToString() == null)
                {
                    return View();
                }

                using (var client = new HttpClient())
                {
                    ActorEntity Actor = null;
                    string cookieToken = Request.Cookies["ACCESS_TOKEN_PELIS"];
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", cookieToken);
                    client.BaseAddress = new Uri(actualHost.HostNameActual + "Actores/");
                    var responseTask = client.GetAsync("BuscarActor/" + id.ToString());
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        result.StatusCode = System.Net.HttpStatusCode.OK;
                        var readTask = result.Content.ReadAsAsync<ActorEntity>();
                        readTask.Wait();

                        Actor = readTask.Result;

                        if (result.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            return View(Actor);
                        }
                        else
                        {
                            return View();
                        }
                    }
                    else
                    {
                        return View();
                    }
                }
            }
            catch (Exception)
            {
                return View();
            }


        }

        [HttpPost]
        public IActionResult EditarActor(ActorEntity actor)
        {
            try
            {
                if (Validacion() == false)
                {
                    return RedirectToAction("Login", "Usuario");
                }
                using (var client = new HttpClient())
                {
                    string cookieToken = Request.Cookies["ACCESS_TOKEN_PELIS"];
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", cookieToken);
                    client.BaseAddress = new Uri(actualHost.HostNameActual + "Actores/EditarActor");
                    var putTaks = client.PutAsJsonAsync<ActorEntity>("EditarActor", actor);
                    putTaks.Wait();

                    var result = putTaks.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        return View();
                    }
                    else
                    {
                        return View();
                    }


                }
            }
            catch (Exception)
            {
                return View();
            }

        }

        [HttpGet]
        public IActionResult EliminarActor(int id)
        {
            try
            {
                if (Validacion() == false)
                {
                    return RedirectToAction("Login", "Usuario");
                }
                if (id.ToString() == "" || id.ToString() == null)
                {
                    return View();
                }

                using (var client = new HttpClient())
                {
                    ActorEntity actor = null;
                    string cookieToken = Request.Cookies["ACCESS_TOKEN_PELIS"];
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", cookieToken);
                    client.BaseAddress = new Uri(actualHost.HostNameActual + "Actores/");
                    var responseTask = client.GetAsync("BuscarActor/" + id.ToString());
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        result.StatusCode = System.Net.HttpStatusCode.OK;
                        var readTask = result.Content.ReadAsAsync<ActorEntity>();
                        readTask.Wait();

                        actor = readTask.Result;

                        if (result.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            return View(actor);
                        }
                        else
                        {
                            return View();
                        }
                    }
                    else
                    {
                        return View();
                    }
                }
            }
            catch (Exception)
            {
                return View();
            }


        }

        [HttpPost]
        public IActionResult EliminarActor(int id, IFormCollection formCollection)
        {
            try
            {
                if (Validacion() == false)
                {
                    return RedirectToAction("Login", "Usuario");
                }
                using (var client = new HttpClient())
                {
                    string cookieToken = Request.Cookies["ACCESS_TOKEN_PELIS"];
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", cookieToken);
                    client.BaseAddress = new Uri(actualHost.HostNameActual + "Actores/");
                    var deleteTask = client.DeleteAsync("EliminarActor/" + id.ToString());


                    var result = deleteTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        return RedirectToAction("ListadoActores");
                    }
                    else
                    {
                        return View();
                    }
                }
            }
            catch (Exception)
            {
                return View();
            }

        }

        [HttpGet]
        public IActionResult DetalleActor(int id)
        {
            try
            {
                if (Validacion() == false)
                {
                    return RedirectToAction("Login", "Usuario");
                }
                if (id.ToString() == "" || id.ToString() == null)
                {
                    return View();
                }

                using (var client = new HttpClient())
                {
                    ActorEntity actor = null;
                    IEnumerable<DetalleActorEntity> lstDetalleActor = null;
                    string cookieToken = Request.Cookies["ACCESS_TOKEN_PELIS"];
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", cookieToken);
                    client.BaseAddress = new Uri(actualHost.HostNameActual + "Actores/");
                    var responseTask = client.GetAsync("BuscarActor/" + id.ToString());
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        result.StatusCode = System.Net.HttpStatusCode.OK;
                        var readTask = result.Content.ReadAsAsync<ActorEntity>();
                        readTask.Wait();

                        actor = readTask.Result;

                        if (result.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            ViewBag.IdActor = actor.IdActor;
                            ViewBag.Nombre = actor.Nombre;
                            ViewBag.FechaNacimiento = actor.FechaNacimiento;
                            ViewBag.Sexo = actor.Sexo;
                            ViewBag.Foto = actor.Foto;
                            
                            using (var client2 = new HttpClient())
                            {
                                client2.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", cookieToken);
                                client2.BaseAddress = new Uri(actualHost.HostNameActual + "DetallesActores/");
                                var responseTask2 = client2.GetAsync("BuscarDetalleActor/" + id.ToString());
                                responseTask2.Wait();

                                var result2 = responseTask2.Result;
                                if (result2.IsSuccessStatusCode)
                                {
                                    var readJob2 = result2.Content.ReadAsAsync<IList<DetalleActorEntity>>();
                                    readJob2.Wait();
                                    lstDetalleActor = readJob2.Result;
                                }
                                else
                                {
                                    lstDetalleActor = Enumerable.Empty<DetalleActorEntity>();

                                }


                            }

                            return View(lstDetalleActor);
                        }
                        else
                        {
                            lstDetalleActor = Enumerable.Empty<DetalleActorEntity>();
                            return View();
                        }
                    }
                    else
                    {
                        lstDetalleActor = Enumerable.Empty<DetalleActorEntity>();
                        return View(lstDetalleActor);
                    }
                }
            }
            catch (Exception)
            {
                IEnumerable<DetalleActorEntity> lstDetalleActor = Enumerable.Empty<DetalleActorEntity>();
                return View(lstDetalleActor);
            }

        }

        [HttpGet]
        public IActionResult GuardarDetalleActor(int IdActor, string NombreActor)
        {
            if (Validacion() == false)
            {
                return RedirectToAction("Login", "Usuario");
            }
            DetalleActorEntity detalleActor = new DetalleActorEntity();
            detalleActor.IdActor = IdActor;
            detalleActor.NombreActor = NombreActor;
            detalleActor.NombrePelicula = "";
            return View(detalleActor);

        }

        [HttpPost]
        public IActionResult GuardarDetalleActor(DetalleActorEntity detalleActor)
        {
            try
            {
                if (Validacion() == false)
                {
                    return RedirectToAction("Login", "Usuario");
                }
                using (var client = new HttpClient())
                {
                    string cookieToken = Request.Cookies["ACCESS_TOKEN_PELIS"];
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", cookieToken);
                    client.BaseAddress = new Uri(actualHost.HostNameActual + "DetallesActores/GuardarDetalleActor");
                    var postJob = client.PostAsJsonAsync<DetalleActorEntity>("GuardarDetalleActor", detalleActor);
                    postJob.Wait();

                    var postResult = postJob.Result;
                    if (postResult.IsSuccessStatusCode)
                    {
                        detalleActor.NombrePelicula = "";
                        return RedirectToAction("GuardarDetalleActor", detalleActor);
                    }
                    else
                    {
                        return View();
                    }

                }

            }
            catch (Exception)
            {

                return View();
            }


        }

        [HttpGet]
        public IActionResult EditarDetalleActor(int id)
        {
            try
            {
                if (Validacion() == false)
                {
                    return RedirectToAction("Login", "Usuario");
                }
                if (id.ToString() == "" || id.ToString() == null)
                {
                    return View();
                }

                using (var client = new HttpClient())
                {
                    DetalleActorEntity detalleActor = null;
                    string cookieToken = Request.Cookies["ACCESS_TOKEN_PELIS"];
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", cookieToken);
                    client.BaseAddress = new Uri(actualHost.HostNameActual + "DetallesActores/");
                    var responseTask = client.GetAsync("DetalleActorId/" + id.ToString());
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        result.StatusCode = System.Net.HttpStatusCode.OK;
                        var readTask = result.Content.ReadAsAsync<DetalleActorEntity>();
                        readTask.Wait();

                        detalleActor = readTask.Result;

                        if (result.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            return View(detalleActor);
                        }
                        else
                        {
                            return View();
                        }
                    }
                    else
                    {
                        return View();
                    }
                }
            }
            catch (Exception)
            {
                return View();
            }


        }

        [HttpPost]
        public IActionResult EditarDetalleActor(DetalleActorEntity detalleActor)
        {
            try
            {
                if (Validacion() == false)
                {
                    return RedirectToAction("Login", "Usuario");
                }
                using (var client = new HttpClient())
                {
                    string cookieToken = Request.Cookies["ACCESS_TOKEN_PELIS"];
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", cookieToken);
                    client.BaseAddress = new Uri(actualHost.HostNameActual + "DetallesActores/EditarDetalleActor");
                    var putTaks = client.PutAsJsonAsync<DetalleActorEntity>("EditarDetalleActor", detalleActor);
                    putTaks.Wait();

                    var result = putTaks.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        return View();
                    }
                    else
                    {
                        return View();
                    }


                }
            }
            catch (Exception)
            {

                return View();
            }


        }

        [HttpGet]
        public IActionResult EliminarDetalleActor(int id)
        {
            try
            {
                if (Validacion() == false)
                {
                    return RedirectToAction("Login", "Usuario");
                }
                if (id.ToString() == "" || id.ToString() == null)
                {
                    return View();
                }

                using (var client = new HttpClient())
                {
                    DetalleActorEntity detalleActor = null;
                    string cookieToken = Request.Cookies["ACCESS_TOKEN_PELIS"];
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", cookieToken);
                    client.BaseAddress = new Uri(actualHost.HostNameActual + "DetallesActores/");
                    var responseTask = client.GetAsync("DetalleActorId/" + id.ToString());
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        result.StatusCode = System.Net.HttpStatusCode.OK;
                        var readTask = result.Content.ReadAsAsync<DetalleActorEntity>();
                        readTask.Wait();

                        detalleActor = readTask.Result;

                        if (result.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            return View(detalleActor);
                        }
                        else
                        {
                            return View();
                        }
                    }
                    else
                    {
                        return View();
                    }
                }
            }
            catch (Exception)
            {
                return View();
            }


        }

        [HttpPost]
        public IActionResult EliminarDetalleActor(int id, IFormCollection formCollection)
        {
            try
            {
                if (Validacion() == false)
                {
                    return RedirectToAction("Login", "Usuario");
                }
                using (var client = new HttpClient())
                {
                    string cookieToken = Request.Cookies["ACCESS_TOKEN_PELIS"];
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", cookieToken);
                    client.BaseAddress = new Uri(actualHost.HostNameActual + "DetallesActores/");
                    var deleteTask = client.DeleteAsync("EliminarDetalleActor/" + id.ToString());

                    var result = deleteTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        return RedirectToAction("ListadoActores");
                    }
                    else
                    {
                        return View();
                    }
                }
            }
            catch (Exception)
            {

                return View();
            }

        }

        public bool Validacion()
        {
            try
            {
                usuario = Request.Cookies["USUARIOWAP"];
                tokenAccess = Request.Cookies["ACCESS_TOKEN_PELIS"];
                if (usuario == null || usuario == "" || tokenAccess == null || tokenAccess == "")
                {
                    return false;
                }

                else
                {
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }



        }


    }
}
