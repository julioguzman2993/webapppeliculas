﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebAppPeliculas.Models.Entity;
using WebAppPeliculas.Servicies;

namespace WebAppPeliculas.Controllers
{
    public class UsuarioController : Controller
    {
        ActualHost actualHost = new ActualHost();
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Login()
        {
            Response.Cookies.Delete("ACCESS_TOKEN_PELIS");
            Response.Cookies.Delete("USUARIOWAP");

            return View();
        }

        [HttpPost]
        public IActionResult Login(LoginEntity login)
        {
            try
            {
                string usuario = "";
                string tipo = "";

                ResponseTokens responseTokens = null;
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(actualHost.HostNameActual + "Usuario/login");
                    var postJob = client.PostAsJsonAsync<LoginEntity>("login", login);
                    postJob.Wait();
                   
                    var postResult = postJob.Result;
                    if (postResult.IsSuccessStatusCode)
                    {

                        var respToken = postResult.Content.ReadAsAsync<ResponseTokens>();
                        respToken.Wait();
                        responseTokens = respToken.Result;

                        CookieOptions option = new CookieOptions();
                        option.Expires = DateTime.Now.AddDays(1);

                        Response.Cookies.Append("ACCESS_TOKEN_PELIS", responseTokens.response, option);
                        

                        usuario = login.Usuario;

                        Response.Cookies.Append("USUARIOWAP", usuario, option);

                        //return View();
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return View();
                    }

                }

            }
            catch (Exception)
            {

                return View();
            }
        }

    }
}
