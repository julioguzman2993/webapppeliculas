﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebAppPeliculas.Models.Entity;
using WebAppPeliculas.Servicies;

namespace WebAppPeliculas.Controllers
{
    public class PeliculasController : Controller
    {
        private string usuario = "";
        private string tokenAccess = "";
        ActualHost actualHost = new ActualHost();
        public IActionResult Index()
        {
            
            IEnumerable<PeliculaEntity> lstPelicula = null;
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(actualHost.HostNameActual + "Peliculas/ListadoPeliculasPublico");
                    var responseTask = client.GetAsync("ListadoPeliculasPublico");

                    responseTask.Wait();

                    var result = responseTask.Result;

                    if (result.IsSuccessStatusCode)
                    {
                        var readJob = result.Content.ReadAsAsync<IList<PeliculaEntity>>();
                        readJob.Wait();
                        lstPelicula = readJob.Result;
                    }
                    else
                    {
                        lstPelicula = Enumerable.Empty<PeliculaEntity>();

                    }




                }


                return View(lstPelicula);
            }
            catch (Exception)
            {

                lstPelicula = Enumerable.Empty<PeliculaEntity>();
                return View(lstPelicula);
            }
        }

        public IActionResult ListadoPeliculas()
        {
            IEnumerable<PeliculaEntity> lstPelicula = null;
            try
            {
                
                if (Validacion() == false)
                {
                    return RedirectToAction("Login", "Usuario");
                }

                using (var client = new HttpClient())
                {
                    string cookieToken = Request.Cookies["ACCESS_TOKEN_PELIS"];
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", cookieToken);
                    client.BaseAddress = new Uri(actualHost.HostNameActual + "Peliculas/ListadoPeliculas");
                    var responseTask = client.GetAsync("ListadoPeliculas");

                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                    {
                        return RedirectToAction("Login", "Usuario");
                    }

                    if (result.IsSuccessStatusCode)
                    {
                        var readJob = result.Content.ReadAsAsync<IList<PeliculaEntity>>();
                        readJob.Wait();
                        lstPelicula = readJob.Result;
                    }
                    else
                    {
                        lstPelicula = Enumerable.Empty<PeliculaEntity>();

                    }
                    
                    


                }


                return View(lstPelicula);
            }
            catch (Exception)
            {

                lstPelicula = Enumerable.Empty<PeliculaEntity>();
                return View(lstPelicula);
            }


        }

        [HttpGet]
        public IActionResult GuardarPelicula()
        {
            if (Validacion() == false)
            {
                return RedirectToAction("Login", "Usuario");
            }
            return View();

        }

        [HttpPost]
        public IActionResult GuardarPelicula(PeliculaEntity pelicula)
        {
            try
            {
                if (Validacion() == false)
                {
                    return RedirectToAction("Login", "Usuario");
                }
                using (var client = new HttpClient())
                {
                    string cookieToken = Request.Cookies["ACCESS_TOKEN_PELIS"];
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", cookieToken);
                    client.BaseAddress = new Uri(actualHost.HostNameActual + "Peliculas/GuardarPelicula");
                    var postJob = client.PostAsJsonAsync<PeliculaEntity>("GuardarPelicula", pelicula);
                    postJob.Wait();

                    var postResult = postJob.Result;
                    if (postResult.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                    {
                        return RedirectToAction("Login", "Usuario");
                    }
                    if (postResult.IsSuccessStatusCode)
                    {
                        return RedirectToAction("GuardarPelicula");
                    }
                    else
                    {
                        return View();
                    }

                }
                
            }
            catch (Exception)
            {

                return View();
            }
            

        }

        [HttpGet]
        public IActionResult EditarPelicula(int id)
        {
            try
            {
                if (Validacion() == false)
                {
                    return RedirectToAction("Login", "Usuario");
                }
                if (id.ToString() == "" || id.ToString() == null)
                {
                    return View();
                }

                using (var client = new HttpClient())
                {
                    PeliculaEntity pelicula = null;
                    string cookieToken = Request.Cookies["ACCESS_TOKEN_PELIS"];
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", cookieToken);
                    client.BaseAddress = new Uri(actualHost.HostNameActual + "Peliculas/");
                    var responseTask = client.GetAsync("BuscarPelicula/" + id.ToString());
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        result.StatusCode = System.Net.HttpStatusCode.OK;
                        var readTask = result.Content.ReadAsAsync<PeliculaEntity>();
                        readTask.Wait();

                        pelicula = readTask.Result;

                        if (result.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            return View(pelicula);
                        }
                        else
                        {
                            return View();
                        }
                    }
                    else
                    {
                        return View();
                    }
                }
            }
            catch (Exception)
            {
                return View();
            }
            

        }

        [HttpPost]
        public IActionResult EditarPelicula(PeliculaEntity pelicula)
        {
            try
            {
                if (Validacion() == false)
                {
                    return RedirectToAction("Login", "Usuario");
                }
                using (var client = new HttpClient())
                {
                    string cookieToken = Request.Cookies["ACCESS_TOKEN_PELIS"];
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", cookieToken);
                    client.BaseAddress = new Uri(actualHost.HostNameActual + "Peliculas/EditarPelicula");
                    var putTaks = client.PutAsJsonAsync<PeliculaEntity>("EditarPelicula", pelicula);
                    putTaks.Wait();

                    var result = putTaks.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        return View();
                    }
                    else
                    {
                        return View();
                    }


                }
            }
            catch (Exception)
            {

                return View();
            }


        }

        [HttpGet]
        public IActionResult EliminarPelicula(int id)
        {
            try
            {
                if (Validacion() == false)
                {
                    return RedirectToAction("Login", "Usuario");
                }
                if (id.ToString() == "" || id.ToString() == null)
                {
                    return View();
                }

                using (var client = new HttpClient())
                {
                    PeliculaEntity pelicula = null;
                    string cookieToken = Request.Cookies["ACCESS_TOKEN_PELIS"];
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", cookieToken);
                    client.BaseAddress = new Uri(actualHost.HostNameActual + "Peliculas/");
                    var responseTask = client.GetAsync("BuscarPelicula/" + id.ToString());
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        result.StatusCode = System.Net.HttpStatusCode.OK;
                        var readTask = result.Content.ReadAsAsync<PeliculaEntity>();
                        readTask.Wait();

                        pelicula = readTask.Result;

                        if (result.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            return View(pelicula);
                        }
                        else
                        {
                            return View();
                        }
                    }
                    else
                    {
                        return View();
                    }
                }
            }
            catch (Exception)
            {
                return View();
            }


        }

        [HttpPost]
        public IActionResult EliminarPelicula(int id, IFormCollection formCollection)
        {
            try
            {
                if (Validacion() == false)
                {
                    return RedirectToAction("Login", "Usuario");
                }
                using (var client = new HttpClient())
                {
                    string cookieToken = Request.Cookies["ACCESS_TOKEN_PELIS"];
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", cookieToken);
                    client.BaseAddress = new Uri(actualHost.HostNameActual + "Peliculas/");
                    var deleteTask = client.DeleteAsync("EliminarPelicula/" + id.ToString());


                    var result = deleteTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        return RedirectToAction("ListadoPeliculas");
                    }
                    else
                    {
                        return View();
                    }
                }
            }
            catch (Exception)
            {

                return View();
            }

        }

        [HttpGet]
        public IActionResult DetallePelicula(int id)
        {
               try
               {
                if (Validacion() == false)
                {
                    return RedirectToAction("Login", "Usuario");
                }
                if (id.ToString() == "" || id.ToString() == null)
                   {
                       return View();
                   }

                   using (var client = new HttpClient())
                   {
                       IEnumerable<DetallePeliculaEntity> lstDetallePelicula = null;
                       PeliculaEntity pelicula = null;
                       string cookieToken = Request.Cookies["ACCESS_TOKEN_PELIS"];
                       client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", cookieToken);
                       client.BaseAddress = new Uri(actualHost.HostNameActual + "Peliculas/");
                       var responseTask = client.GetAsync("BuscarPelicula/" + id.ToString());
                       responseTask.Wait();

                       var result = responseTask.Result;
                       if (result.IsSuccessStatusCode)
                       {
                           result.StatusCode = System.Net.HttpStatusCode.OK;
                           var readTask = result.Content.ReadAsAsync<PeliculaEntity>();
                           readTask.Wait();

                           pelicula = readTask.Result;

                           if (result.StatusCode == System.Net.HttpStatusCode.OK)
                           {

                            ViewBag.IdPelicula = pelicula.IdPelicula;
                            ViewBag.Titulo = pelicula.Titulo;
                            ViewBag.Genero = pelicula.Genero;
                            ViewBag.FechaEstreno = pelicula.FechaEstreno;
                            ViewBag.Foto = pelicula.Foto;
                            
                            using (var client2 = new HttpClient())
                            {
                                client2.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", cookieToken);
                                client2.BaseAddress = new Uri(actualHost.HostNameActual + "DetallesPeliculas/");
                                var responseTask2 = client2.GetAsync("BuscarDetallePelicula/" + id.ToString());
                                responseTask2.Wait();

                                var result2 = responseTask2.Result;
                                if (result2.IsSuccessStatusCode)
                                {
                                    var readJob2 = result2.Content.ReadAsAsync<IList<DetallePeliculaEntity>>();
                                    readJob2.Wait();
                                    lstDetallePelicula = readJob2.Result;
                                }
                                else
                                {
                                    lstDetallePelicula = Enumerable.Empty<DetallePeliculaEntity>();

                                }


                            }

                            return View(lstDetallePelicula);
                        }
                           else
                           {
                            lstDetallePelicula = Enumerable.Empty<DetallePeliculaEntity>();
                            return View(lstDetallePelicula);
                           }
                       }
                       else
                       {
                         lstDetallePelicula = Enumerable.Empty<DetallePeliculaEntity>();
                         return View(lstDetallePelicula);
                       }
                   }
               }
               catch (Exception)
               {
                IEnumerable<DetallePeliculaEntity> lstDetallePelicula = Enumerable.Empty<DetallePeliculaEntity>();
                return View(lstDetallePelicula);
               }
               
        }

        [HttpGet]
        public IActionResult GuardarDetallePelicula(int IdPelicula, string NombrePelicula)
        {
            if (Validacion() == false)
            {
                return RedirectToAction("Login", "Usuario");
            }
            DetallePeliculaEntity detallePelicula = new DetallePeliculaEntity();
            detallePelicula.IdPelicula = IdPelicula;
            detallePelicula.NombrePelicula = NombrePelicula;
            detallePelicula.NombreActor = "";
            return View(detallePelicula);

        }

        [HttpPost]
        public IActionResult GuardarDetallePelicula(DetallePeliculaEntity detallePelicula)
        {
            try
            {
                if (Validacion() == false)
                {
                    return RedirectToAction("Login", "Usuario");
                }
                using (var client = new HttpClient())
                {
                    string cookieToken = Request.Cookies["ACCESS_TOKEN_PELIS"];
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", cookieToken);
                    client.BaseAddress = new Uri(actualHost.HostNameActual + "DetallesPeliculas/DetallePelicula");
                    var postJob = client.PostAsJsonAsync<DetallePeliculaEntity>("GuardarDetallePelicula", detallePelicula);
                    postJob.Wait();

                    var postResult = postJob.Result;
                    if (postResult.IsSuccessStatusCode)
                    {
                        detallePelicula.NombreActor = "";
                        return RedirectToAction("GuardarDetallePelicula",detallePelicula);
                    }
                    else
                    {
                        return View();
                    }

                }

            }
            catch (Exception)
            {

                return View();
            }


        }

        [HttpGet]
        public IActionResult EditarDetallePelicula(int id)
        {
            try
            {
                if (Validacion() == false)
                {
                    return RedirectToAction("Login", "Usuario");
                }
                if (id.ToString() == "" || id.ToString() == null)
                {
                    return View();
                }

                using (var client = new HttpClient())
                {
                    DetallePeliculaEntity detallePelicula = null;
                    string cookieToken = Request.Cookies["ACCESS_TOKEN_PELIS"];
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", cookieToken);
                    client.BaseAddress = new Uri(actualHost.HostNameActual + "DetallesPeliculas/");
                    var responseTask = client.GetAsync("BuscarDetallePeliculaId/" + id.ToString());
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        result.StatusCode = System.Net.HttpStatusCode.OK;
                        var readTask = result.Content.ReadAsAsync<DetallePeliculaEntity>();
                        readTask.Wait();

                        detallePelicula = readTask.Result;

                        if (result.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            return View(detallePelicula);
                        }
                        else
                        {
                            return View();
                        }
                    }
                    else
                    {
                        return View();
                    }
                }
            }
            catch (Exception)
            {
                return View();
            }


        }

        [HttpPost]
        public IActionResult EditarDetallePelicula(DetallePeliculaEntity detallePelicula)
        {
            try
            {
                if (Validacion() == false)
                {
                    return RedirectToAction("Login", "Usuario");
                }
                using (var client = new HttpClient())
                {
                    string cookieToken = Request.Cookies["ACCESS_TOKEN_PELIS"];
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", cookieToken);
                    client.BaseAddress = new Uri(actualHost.HostNameActual + "DetallesPeliculas/EditarDetallePelicula");
                    var putTaks = client.PutAsJsonAsync<DetallePeliculaEntity>("EditarDetallePelicula", detallePelicula);
                    putTaks.Wait();

                    var result = putTaks.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        return View();
                    }
                    else
                    {
                        return View();
                    }


                }
            }
            catch (Exception)
            {

                return View();
            }


        }

        [HttpGet]
        public IActionResult EliminarDetallePelicula(int id)
        {
            try
            {
                if (Validacion() == false)
                {
                    return RedirectToAction("Login", "Usuario");
                }
                if (id.ToString() == "" || id.ToString() == null)
                {
                    return View();
                }

                using (var client = new HttpClient())
                {
                    DetallePeliculaEntity detallePelicula = null;
                    string cookieToken = Request.Cookies["ACCESS_TOKEN_PELIS"];
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", cookieToken);
                    client.BaseAddress = new Uri(actualHost.HostNameActual + "DetallesPeliculas/");
                    var responseTask = client.GetAsync("BuscarDetallePeliculaId/" + id.ToString());
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        result.StatusCode = System.Net.HttpStatusCode.OK;
                        var readTask = result.Content.ReadAsAsync<DetallePeliculaEntity>();
                        readTask.Wait();

                        detallePelicula = readTask.Result;

                        if (result.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            return View(detallePelicula);
                        }
                        else
                        {
                            return View();
                        }
                    }
                    else
                    {
                        return View();
                    }
                }
            }
            catch (Exception)
            {
                return View();
            }


        }

        [HttpPost]
        public IActionResult EliminarDetallePelicula(int id, IFormCollection formCollection)
        {
            try
            {
                if (Validacion() == false)
                {
                    return RedirectToAction("Login", "Usuario");
                }
                using (var client = new HttpClient())
                {
                    string cookieToken = Request.Cookies["ACCESS_TOKEN_PELIS"];
                      client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", cookieToken);
                    client.BaseAddress = new Uri(actualHost.HostNameActual + "DetallesPeliculas/");
                    var deleteTask = client.DeleteAsync("EliminarDetallePelicula/" + id.ToString());

                    var result = deleteTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        return RedirectToAction("ListadoPeliculas");
                    }
                    else
                    {
                        return View();
                    }
                }
            }
            catch (Exception)
            {

                return View();
            }

        }

        public bool Validacion()
        {
            try
            {
                usuario = Request.Cookies["USUARIOWAP"];
                tokenAccess = Request.Cookies["ACCESS_TOKEN_PELIS"];
                if (usuario == null || usuario == "" || tokenAccess == null || tokenAccess == "")
                {
                    return false;
                }

                else
                {
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
            
            

        }

  

    }
}
