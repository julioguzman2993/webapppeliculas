﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAppPeliculas.Models.Entity
{
    public class PeliculaEntity
    {
        [Required]
        [Key]
        public int IdPelicula { get; set; }
        [Required]
        public string Titulo { get; set; }
        [Required]
        public string Genero { get; set; }
        [Required]
        public DateTime FechaEstreno { get; set; }
        [Required]
        public string Foto { get; set; }
    }
}
