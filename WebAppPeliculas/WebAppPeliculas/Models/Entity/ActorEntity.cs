﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAppPeliculas.Models.Entity
{
    public class ActorEntity
    {
        [Key]
        [Required]
        public int IdActor { get; set; }
        [Required]
        public string Nombre { get; set; }
        [Required]
        public DateTime FechaNacimiento { get; set; }
        [Required]
        public string Sexo { get; set; }
        [Required]
        public string Foto { get; set; }
    }
}
