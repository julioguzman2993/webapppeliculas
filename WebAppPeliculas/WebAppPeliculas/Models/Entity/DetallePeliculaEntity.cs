﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAppPeliculas.Models.Entity
{
    public class DetallePeliculaEntity
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public int IdPelicula { get; set; }

        [Display(Name = "Pelicula")]
        [Required]
        public string NombrePelicula { get; set; }

        [Display(Name = "Actor")]
        [Required]
        public string NombreActor { get; set; }
    }
}
