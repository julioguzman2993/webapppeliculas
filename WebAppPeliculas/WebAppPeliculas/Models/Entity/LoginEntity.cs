﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAppPeliculas.Models.Entity
{
    public class LoginEntity
    {
        public string Usuario { get; set; }
        public string Clave { get; set; }
    }
}
