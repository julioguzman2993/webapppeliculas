﻿using ApiRestPeliculas.DataAcces;
using ApiRestPeliculas.Models.Abstractions;
using ApiRestPeliculas.Models.Response;
using ApiRestPeliculas.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiRestPeliculas.Aplicacion.Dto
{
    public class ActorService
    {
        readonly IActorDA actorDA;

        public ActorService(MyDBContext context)
        {
            actorDA = new ActorDA(context);
        }

        public IEnumerable<ActorViewModel> ListadoActores()
        {
            return actorDA.ListadoActores();
        }

        public object BuscarActor(int id)
        {
            return actorDA.BuscarActor(id);
        }

        public MyResponse GuardarActor(ActorViewModel actorModel)
        {
            return actorDA.GuardarActor(actorModel);
        }

        public MyResponse EditarActor(ActorViewModel actorModel)
        {
            return actorDA.EditarActor(actorModel);
        }

        public MyResponse EliminarActor(int id)
        {
            return actorDA.EliminarActor(id);
        }

    }
}
