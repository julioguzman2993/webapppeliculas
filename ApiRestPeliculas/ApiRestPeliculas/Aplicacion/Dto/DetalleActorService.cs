﻿using ApiRestPeliculas.DataAcces;
using ApiRestPeliculas.Models.Abstractions;
using ApiRestPeliculas.Models.Response;
using ApiRestPeliculas.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiRestPeliculas.Aplicacion.Dto
{
    public class DetalleActorService
    {
        readonly IDetalleActorDA detalleActorDA;

        public DetalleActorService(MyDBContext context)
        {
            detalleActorDA = new DetalleActorDA(context);
        }

        public IEnumerable<DetalleActorViewModel> ListadoDetallesActores()
        {
            return detalleActorDA.ListadoDetallesActores();
        }

        public IEnumerable<DetalleActorViewModel> BuscarDetalleActor(int id)
        {
            return detalleActorDA.BuscarDetalleActor(id);
        }

        public object BuscarDetalleActorId(int id)
        {
            return detalleActorDA.BuscarDetalleActorId(id);
        }

        public MyResponse GuardarDetalleActor(DetalleActorViewModel detalleActorModel)
        {
            return detalleActorDA.GuardarDetalleActor(detalleActorModel);
        }

        public MyResponse EditarDetalleActor(DetalleActorViewModel detalleActorModel)
        {
            return detalleActorDA.EditarDetalleActor(detalleActorModel);
        }

        public MyResponse EliminarDetalleActor(int id)
        {
            return detalleActorDA.EliminarDetalleActor(id);
        }

    }
}
