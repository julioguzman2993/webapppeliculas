﻿using ApiRestPeliculas.DataAcces;
using ApiRestPeliculas.Models.Abstractions;
using ApiRestPeliculas.Models.Response;
using ApiRestPeliculas.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiRestPeliculas.Aplicacion.Dto
{
    public class PeliculaService
    {
        readonly IPeliculaDA peliculaDA;

        public PeliculaService(MyDBContext context)
        {
            peliculaDA = new PeliculaDA(context);
        }

        public IEnumerable<PeliculaViewModel> ListadoPeliculas()
        {
            return peliculaDA.ListadoPeliculas();
        }

        public object BuscarPelicula(int id)
        {
            return peliculaDA.BuscarPelicula(id);
        }

        public MyResponse GuardarPelicula(PeliculaViewModel peliculaModel)
        {
            return peliculaDA.GuardarPelicula(peliculaModel);
        }

        public MyResponse EditarPelicula(PeliculaViewModel model)
        {
            return peliculaDA.EditarPelicula(model);
        }

        public MyResponse EliminarPelicula(int id)
        {
            return peliculaDA.EliminarPelicula(id);
        }

    }
}
