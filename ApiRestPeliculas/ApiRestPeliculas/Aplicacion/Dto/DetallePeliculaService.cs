﻿using ApiRestPeliculas.DataAcces;
using ApiRestPeliculas.Models.Abstractions;
using ApiRestPeliculas.Models.Response;
using ApiRestPeliculas.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiRestPeliculas.Aplicacion.Dto
{
    public class DetallePeliculaService
    {
        readonly IDetallePeliculaDA detallePeliculaDA;

        public DetallePeliculaService(MyDBContext context)
        {
            detallePeliculaDA = new DetallePeliculaDA(context);
        }

        public IEnumerable<DetallePeliculaViewModel> ListadoDetallePeliculas()
        {
            return detallePeliculaDA.ListadoDetallePeliculas();
        }

        public IEnumerable<DetallePeliculaViewModel> BuscarDetallePelicula(int id)
        {
            return detallePeliculaDA.BuscarDetallePelicula(id);
        }

        public object BuscarDetallePeliculaId(int id)
        {
            return detallePeliculaDA.BuscarDetallePeliculaId(id);
        }

        public MyResponse GuardarDetallePelicula(DetallePeliculaViewModel detallepeliculaModel)
        {
            return detallePeliculaDA.GuardarDetallePelicula(detallepeliculaModel);
        }

        public MyResponse EditarDetallePelicula(DetallePeliculaViewModel detalleModel)
        {
            return detallePeliculaDA.EditarDetallePelicula(detalleModel);
        }

        public MyResponse EliminarDetallePelicula(int id)
        {
            return detallePeliculaDA.EliminarDetallePelicula(id);
        }

    }
}
