﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiRestPeliculas.Aplicacion.Dto;
using ApiRestPeliculas.DataAcces;
using ApiRestPeliculas.Models.Entity;
using ApiRestPeliculas.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApiRestPeliculas.Controllers
{
    //Controlador para los servicios de detalles de peliculas
    [ApiController]
    [Route("api/DetallesPeliculas/")]
    public class DetallesPeliculasController : ControllerBase
    {
        private MyDBContext db;
        private DetallePeliculaService detallePeliculaService;

        public DetallesPeliculasController(MyDBContext context)
        {
            db = context;
            detallePeliculaService = new DetallePeliculaService(context);
        }

        [Authorize]
        [HttpGet]
        [Route("ListadoDetallePeliculas")]
        public IEnumerable<DetallePeliculaViewModel> ListadoDetallePeliculas()
        {
            try
            {
                var detallePeliculaList = new DetallePeliculaService(db).ListadoDetallePeliculas();

                return detallePeliculaList;
            }
            catch (Exception)
            {
                List<DetallePeliculaViewModel> lst = null;
                return lst;
            }
        }

        [Authorize]
        [HttpGet]
        [Route("BuscarDetallePelicula/{id}")]
        public IEnumerable<DetallePeliculaViewModel> BuscarDetallePelicula(int id)
        {
            try
            {
                var detallePeliculaList = new DetallePeliculaService(db).BuscarDetallePelicula(id);

                return detallePeliculaList;

            }
            catch (Exception)
            {
                List<DetallePeliculaViewModel> lst = null;
                return lst;
            }
        }

        [Authorize]
        [HttpGet]
        [Route("BuscarDetallePeliculaId/{id}")]
        public IActionResult BuscarDetallePeliculaId(int id)
        {
            try
            {
                var detallePelicula = new DetallePeliculaService(db).BuscarDetallePeliculaId(id);

                return Ok(detallePelicula);
              
            }
            catch (Exception)
            {
                return BadRequest();
            }


        }

        [Authorize]
        [HttpPost]
        [Route("GuardarDetallePelicula")]
        public IActionResult GuardarDetallePelicula(DetallePeliculaViewModel detallePeliculaModel)
        {
            try
            {
                var respPelicula = new DetallePeliculaService(db).GuardarDetallePelicula(detallePeliculaModel);

                return Ok(respPelicula);
       
            }
            catch (Exception)
            {
                return BadRequest();

            }



        }

        [Authorize]
        [HttpPut]
        [Route("EditarDetallePelicula")]
        public IActionResult EditarDetallePelicula(DetallePeliculaViewModel detallePeliculaModel)
        {
            try
            {
                var respPelicula = new DetallePeliculaService(db).EditarDetallePelicula(detallePeliculaModel);

                return Ok(respPelicula);
         
            }
            catch (Exception)
            {
                return BadRequest();

            }


        }

        [Authorize]
        [HttpDelete]
        [Route("EliminarDetallePelicula/{id}")]
        public IActionResult EliminarDetallePelicula(int id)
        {
            try
            {
                var respPelicula = new DetallePeliculaService(db).EliminarDetallePelicula(id);

                return Ok(respPelicula);
        
            }
            catch (Exception)
            {
                return BadRequest();
            }


        }

    }
}
