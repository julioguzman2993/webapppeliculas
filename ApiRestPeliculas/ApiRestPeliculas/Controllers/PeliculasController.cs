﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiRestPeliculas.Aplicacion.Dto;
using ApiRestPeliculas.DataAcces;
using ApiRestPeliculas.Models.Entity;
using ApiRestPeliculas.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApiRestPeliculas.Controllers
{
    //Controlador para los servicios de peliculas
    [ApiController]
    [Route("api/Peliculas/")]
    public class PeliculasController : ControllerBase
    {
        private MyDBContext db;
        private PeliculaService peliculaService;

        public PeliculasController(MyDBContext context)
        {
            db = context;
            peliculaService = new PeliculaService(context);
        }

        [Authorize]
        [HttpGet]
        [Route("ListadoPeliculas")]
        public IEnumerable<PeliculaViewModel> ListadoPeliculas()
        {
            try
            {
                var peliculaList = peliculaService.ListadoPeliculas();

                return peliculaList;

            }
            catch (Exception)
            {
                List<PeliculaViewModel> lst = null;
                return lst;
            }
        }

        [Authorize]
        [HttpGet]
        [Route("BuscarPelicula/{id}")]
        public IActionResult BuscarPelicula(int id)
        {
            try
            {
                var Pelicula = peliculaService.BuscarPelicula(id);
                
                return Ok(Pelicula);

            }
            catch (Exception)
            {
                return BadRequest();
            }


        }

        [Authorize]
        [HttpPost]
        [Route("GuardarPelicula")]
        public IActionResult GuardarPelicula(PeliculaViewModel peliculaModel)
        {
            try
            {
                var respPelicula = peliculaService.GuardarPelicula(peliculaModel);

                return Ok(respPelicula);
                
            }
            catch (Exception)
            {
                return BadRequest();

            }

            

        }

        [Authorize]
        [HttpPut]
        [Route("EditarPelicula")]
        public IActionResult EditarPelicula(PeliculaViewModel model)
        {
            try
            {
                var respPelicula = peliculaService.EditarPelicula(model);

                return Ok(respPelicula);
            }
            catch (Exception)
            {
                return BadRequest();

            }
            

        }

        [Authorize]
        [HttpDelete]
        [Route("EliminarPelicula/{id}")]
        public IActionResult EliminarPelicula(int id)
        {
            try
            {
                var respPelicula = peliculaService.EliminarPelicula(id);

                return Ok(respPelicula);
            }
            catch (Exception)
            {
                return BadRequest();
            }
            
        }

        [HttpGet]
        [Route("ListadoPeliculasPublico")]
        public IEnumerable<PeliculaViewModel> ListadoPeliculasPublico()
        {
            try
            {
                var peliculaList = peliculaService.ListadoPeliculas();

                return peliculaList;

            }
            catch (Exception)
            {
                List<PeliculaViewModel> lst = null;
                return lst;
            }
        }


    }
}
