﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiRestPeliculas.Aplicacion.Dto;
using ApiRestPeliculas.DataAcces;
using ApiRestPeliculas.Models.Entity;
using ApiRestPeliculas.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApiRestPeliculas.Controllers
{
    //Controlador para los servicios de actores
    [ApiController]
    [Route("api/Actores/")]
    public class ActoresController : ControllerBase
    {
        private MyDBContext db;
        private ActorService actorService;

        public ActoresController(MyDBContext context)
        {
            db = context;
            actorService = new ActorService(context);
        }

        [Authorize]
        [HttpGet]
        [Route("ListadoActores")]
        public IEnumerable<ActorViewModel> ListadoActores()
        {
            try
            {
                var actorList = actorService.ListadoActores();

                return actorList;
                
            }
            catch (Exception)
            {
                List<ActorViewModel> lst = null;
                return lst;
            }
        }

        [Authorize]
        [HttpGet]
        [Route("BuscarActor/{id}")]
        public IActionResult BuscarActor(int id)
        {
            try
            {
                var Actor = actorService.BuscarActor(id);

                return Ok(Actor);
            
            }
            catch (Exception)
            {
                return BadRequest();
            }


        }

        [Authorize]
        [HttpPost]
        [Route("GuardarActor")]
        public IActionResult GuardarActor(ActorViewModel actorModel)
        {
            try
            {
                var respActor = actorService.GuardarActor(actorModel);

                return Ok(respActor);
   
            }
            catch (Exception)
            {
                return BadRequest();

            }

        }

        [Authorize]
        [HttpPut]
        [Route("EditarActor")]
        public IActionResult EditarActor(ActorViewModel actorModel)
        {
            try
            {
                var respActor = actorService.EditarActor(actorModel);

                return Ok(respActor);

            }
            catch (Exception)
            {
                return BadRequest();

            }

        }

        [Authorize]
        [HttpDelete]
        [Route("EliminarActor/{id}")]
        public IActionResult EliminarActor(int id)
        {
            try
            {
                var respActor = actorService.EliminarActor(id);

                return Ok(respActor);

            }
            catch (Exception)
            {
                return BadRequest();
            }

        }

        [HttpGet]
        [Route("ListadoActoresPublico")]
        public IEnumerable<ActorViewModel> ListadoActoresPublico()
        {
            try
            {
                var actorList = actorService.ListadoActores();

                return actorList;

            }
            catch (Exception)
            {
                List<ActorViewModel> lst = null;
                return lst;
            }
        }

    }
}
