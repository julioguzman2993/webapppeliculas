﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiRestPeliculas.Aplicacion.Dto;
using ApiRestPeliculas.DataAcces;
using ApiRestPeliculas.Models.Entity;
using ApiRestPeliculas.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApiRestPeliculas.Controllers
{
    //Controlador para los servicios de detalles de actores
    [ApiController]
    [Route("api/DetallesActores/")]
    public class DetallesActoresController : ControllerBase
    {
        private DetalleActorService detalleActorService;

        public DetallesActoresController(MyDBContext context)
        {
            detalleActorService = new DetalleActorService(context);
        }

        [Authorize]
        [HttpGet]
        [Route("ListadoDetallesActores")]
        public IEnumerable<DetalleActorViewModel> ListadoDetallesActores()
        {
            try
            {
                var detalleActorList = detalleActorService.ListadoDetallesActores();

                return detalleActorList;
       
            }
            catch (Exception)
            {
                List<DetalleActorViewModel> lst = null;
                return lst;
            }
        }

        [Authorize]
        [HttpGet]
        [Route("BuscarDetalleActor/{id}")]
        public IEnumerable<DetalleActorViewModel> BuscarDetalleActor(int id)
        {
            try
            {
                var detalleActorList = detalleActorService.BuscarDetalleActor(id);

                return detalleActorList;

            }
            catch (Exception)
            {
                List<DetalleActorViewModel> lst = null;
                return lst;
            }


        }

        [Authorize]
        [HttpGet]
        [Route("DetalleActorId/{id}")]
        public IActionResult BuscarDetalleActorId(int id)
        {
            try
            {
                var detalleActorList = detalleActorService.BuscarDetalleActorId(id);

                return Ok(detalleActorList);
  
            }
            catch (Exception)
            {
                return BadRequest();
            }


        }

        [Authorize]
        [HttpPost]
        [Route("GuardarDetalleActor")]
        public IActionResult GuardarDetalleActor(DetalleActorViewModel detalleActorModel)
        {
            try
            {
                var respActor = detalleActorService.GuardarDetalleActor(detalleActorModel);

                return Ok(respActor);

            }
            catch (Exception)
            {
                return BadRequest();

            }



        }

        [Authorize]
        [HttpPut]
        [Route("EditarDetalleActor")]
        public IActionResult EditarDetalleActor(DetalleActorViewModel detalleActorModel)
        {
            try
            {
                var respActor = detalleActorService.EditarDetalleActor(detalleActorModel);

                return Ok(respActor);

            }
            catch (Exception)
            {
                return BadRequest();

            }


        }

        [Authorize]
        [HttpDelete]
        [Route("EliminarDetalleActor/{id}")]
        public IActionResult EliminarDetalleActor(int id)
        {
            try
            {
                var respActor = detalleActorService.EliminarDetalleActor(id);

                return Ok(respActor);
 
            }
            catch (Exception)
            {
                return BadRequest();
            }


        }

    }
}
