﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using ApiRestPeliculas.DataAcces;
using ApiRestPeliculas.Models.ObjectValues;
using ApiRestPeliculas.Models.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;

namespace ApiRestPeliculas.Controllers
{
    //Controlador para los servicios de usuarios
    [Route("api/Usuario/")]
    [ApiController]
    public class UsuarioController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<UsuarioController> _logger;

        private MyDBContext db;

        public UsuarioController(IConfiguration configuration, ILogger<UsuarioController> logger, MyDBContext context)
        {
            _configuration = configuration;
            _logger = logger;
            db = context;
        }

        [HttpPost]
        [Route("login")]
        public IActionResult Login(LoginViewModel login)
        {
            try
            {
                //Verifica el usuario y contraseña
                if (login.Usuario != "" && login.Clave != null)
                {
                    var productos = from d in db.Usuarios where d.Nombre == login.Usuario && d.Clave == login.Clave select d;
                    if (productos.FirstOrDefault() == null)
                    {
                        return BadRequest("Usuario/Contraseña incorrectos");
                    }

                    if (productos.FirstOrDefault().Nombre == login.Usuario && productos.FirstOrDefault().Clave == login.Clave)
                    {
                        var token = GenerarToken(login);

                        return Ok(new
                        {
                            response = new JwtSecurityTokenHandler().WriteToken(token)
                        });
                    }
                    else
                    {
                        return BadRequest("Usuario/Contraseña incorrectos");
                    }

                }
                else if (login.Usuario != "" && login.Clave != null)
                {
                    var productos = from d in db.Usuarios where d.Email == login.Usuario && d.Clave == login.Clave select d;
                    if (productos.FirstOrDefault() == null)
                    {
                        return BadRequest("Usuario/Contraseña incorrectos");
                    }
                    if (productos.FirstOrDefault().Nombre == login.Usuario && productos.FirstOrDefault().Clave == login.Clave)
                    {
                        var token = GenerarToken(login);

                        return Ok(new
                        {
                            response = new JwtSecurityTokenHandler().WriteToken(token)
                        });
                    }
                    else
                    {
                        return BadRequest("Usuario/Contraseña incorrectos");
                    }
                }
                else
                {
                    return BadRequest("Usuario/Contraseña incorrectos");
                }

            }
            catch (Exception e)
            {
                _logger.LogError("Login: " + e.Message, e);
                return StatusCode((int)System.Net.HttpStatusCode.InternalServerError, e.Message);
            }
        }

        //metodo para generar token con JWT
        private JwtSecurityToken GenerarToken(LoginViewModel login)
        {
            string ValidIssuer = _configuration["ApiAuth:Issuer"];
            string ValidAudience = _configuration["ApiAuth:Audience"];
            SymmetricSecurityKey IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["ApiAuth:SecretKey"]));

            //La fecha de expiracion sera el mismo dia a las 12 de la noche
            DateTime dtFechaExpiraToken;
            DateTime now = DateTime.Now;
            dtFechaExpiraToken = new DateTime(now.Year, now.Month, now.Day, 23, 59, 59, 999);

            //Agregamos los claim nuestros
            var claims = new[]
            {
                new Claim(Constantes.JWT_CLAIM_USUARIO, login.Usuario)
            };

            return new JwtSecurityToken
            (
                issuer: ValidIssuer,
                audience: ValidAudience,
                claims: claims,
                expires: dtFechaExpiraToken,
                notBefore: now,
                signingCredentials: new SigningCredentials(IssuerSigningKey, SecurityAlgorithms.HmacSha256)
            );
        }
    }
}
