﻿using ApiRestPeliculas.Models.Abstractions;
using ApiRestPeliculas.Models.Entity;
using ApiRestPeliculas.Models.Response;
using ApiRestPeliculas.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiRestPeliculas.DataAcces
{
    public class DetalleActorDA : IDetalleActorDA
    {
        private MyDBContext db;

        public DetalleActorDA(MyDBContext context)
        {
            db = context;
        }

        public IEnumerable<DetalleActorViewModel> ListadoDetallesActores()
        {
            try
            {
                List<DetalleActorViewModel> lst = (from d in db.DetalleActores
                                                   select new DetalleActorViewModel
                                                   {
                                                       Id = d.Id,
                                                       IdActor = d.IdActor,
                                                       NombreActor = d.NombreActor,
                                                       NombrePelicula = d.NombrePelicula
                                                   }).ToList();


                return lst;
            }
            catch (Exception)
            {
                List<DetalleActorViewModel> lst = null;
                return lst;
            }
        }

        public IEnumerable<DetalleActorViewModel> BuscarDetalleActor(int id)
        {
            try
            {
                List<DetalleActorViewModel> lst = (from d in db.DetalleActores
                                                   where d.IdActor == id
                                                   select new DetalleActorViewModel
                                                   {
                                                       Id = d.Id,
                                                       IdActor = d.IdActor,
                                                       NombreActor = d.NombreActor,
                                                       NombrePelicula = d.NombrePelicula
                                                   }).ToList();


                return lst;
            }
            catch (Exception)
            {
                List<DetalleActorViewModel> lst = null;
                return lst;
            }


        }

        public object BuscarDetalleActorId(int id)
        {
            MyResponse resp = new MyResponse();
            var detalleActor = db.DetalleActores.Find(id);
            try
            {
                if (detalleActor == null)
                {
                    resp.Success = false;
                    resp.Message = "Detalle de Actor no existe!";
                    resp.Data = detalleActor;
                    return resp;
                }

                return detalleActor;
            }
            catch (Exception)
            {
                resp.Success = false;
                resp.Message = "Error al tratar de buscar detalle de actor!";
                resp.Data = detalleActor;
                return resp;
            }


        }

        public MyResponse GuardarDetalleActor(DetalleActorViewModel detalleActorModel)
        {
            MyResponse resp = new MyResponse();
            try
            {
                DetalleActorEntity detalleActor = new DetalleActorEntity();
                detalleActor.IdActor = detalleActorModel.IdActor;
                detalleActor.NombreActor = detalleActorModel.NombreActor;
                detalleActor.NombrePelicula = detalleActorModel.NombrePelicula;
                db.DetalleActores.Add(detalleActor);
                db.SaveChanges();
                resp.Success = true;
                resp.Message = "Detalle de Actor Guardada con Exito!";
                return resp;
            }
            catch (Exception)
            {
                resp.Success = false;
                resp.Message = "Error al tratar guardar Detalle de Actor";
                return resp;

            }

        }

        public MyResponse EditarDetalleActor(DetalleActorViewModel detalleActorModel)
        {
            MyResponse resp = new MyResponse();
            try
            {
                DetalleActorEntity detalleActor = new DetalleActorEntity();
                detalleActor.Id = detalleActorModel.Id;
                detalleActor.IdActor = detalleActorModel.IdActor;
                detalleActor.NombreActor = detalleActorModel.NombreActor;
                detalleActor.NombrePelicula = detalleActorModel.NombrePelicula;
                db.DetalleActores.Update(detalleActor);
                db.SaveChanges();
                resp.Success = true;
                resp.Message = "Detalle de Actor Editado con Exito!";
                return resp;
            }
            catch (Exception)
            {
                resp.Success = false;
                resp.Message = "Error al tratar editar Detalle de Actor";
                return resp;

            }

        }

        public MyResponse EliminarDetalleActor(int id)
        {
            MyResponse resp = new MyResponse();
            try
            {
                DetalleActorEntity detalleActor = new DetalleActorEntity();
                detalleActor.Id = id;
                db.DetalleActores.Remove(detalleActor);
                db.SaveChanges();
                resp.Success = true;
                resp.Message = "Detalle de Actor Eliminado con Exito!";
                return resp;
            }
            catch (Exception)
            {
                resp.Success = false;
                resp.Message = "Error al tratar eliminar Detalle de Actor";
                return resp;
            }


        }

    }
}
