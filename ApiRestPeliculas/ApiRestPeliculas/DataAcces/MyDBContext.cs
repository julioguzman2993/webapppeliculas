﻿using ApiRestPeliculas.Models.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiRestPeliculas.DataAcces
{
    public class MyDBContext: DbContext
    {
        public MyDBContext(DbContextOptions<MyDBContext> options) : base(options) { }

        public DbSet<PeliculaEntity> Peliculas { get; set; }
        public DbSet<DetallePeliculaEntity> DetallePeliculas { get; set; }
        public DbSet<ActorEntity> Actores { get; set; }
        public DbSet<DetalleActorEntity> DetalleActores { get; set; }
        public DbSet<UsuarioEntity> Usuarios { get; set; }
    }
}
