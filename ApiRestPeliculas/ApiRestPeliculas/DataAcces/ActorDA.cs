﻿using ApiRestPeliculas.Models.Abstractions;
using ApiRestPeliculas.Models.Entity;
using ApiRestPeliculas.Models.Response;
using ApiRestPeliculas.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiRestPeliculas.DataAcces
{
    public class ActorDA : IActorDA
    {
        private MyDBContext db;

        public ActorDA(MyDBContext context)
        {
            db = context;
        }

        public IEnumerable<ActorViewModel> ListadoActores()
        {
            try
            {
                List<ActorViewModel> lst = (from d in db.Actores
                                            select new ActorViewModel
                                            {
                                                IdActor = d.IdActor,
                                                Nombre = d.Nombre,
                                                FechaNacimiento = d.FechaNacimiento,
                                                Sexo = d.Sexo,
                                                Foto = d.Foto
                                            }).ToList();


                return lst;
            }
            catch (Exception)
            {
                List<ActorViewModel> lst = null;
                return lst;
            }
        }

        public object BuscarActor(int id)
        {
            MyResponse resp = new MyResponse();
            var Actor = db.Actores.Find(id);
            try
            {
                if (Actor == null)
                {
                    resp.Success = false;
                    resp.Message = "Actor no existe!";
                    resp.Data = Actor;
                    return resp;
                }
                return Actor;
            }
            catch (Exception)
            {
                resp.Success = false;
                resp.Message = "Error al tratar de buscar Actor!";
                resp.Data = Actor;
                return resp;
            }


        }

        public MyResponse GuardarActor(ActorViewModel actorModel)
        {
            MyResponse resp = new MyResponse();
            try
            {
                ActorEntity Actor = new ActorEntity();
                Actor.Nombre = actorModel.Nombre;
                Actor.FechaNacimiento = actorModel.FechaNacimiento;
                Actor.Sexo = actorModel.Sexo;
                Actor.Foto = actorModel.Foto;
                db.Actores.Add(Actor);
                db.SaveChanges();
                resp.Success = true;
                resp.Message = "Actor Guardado con Exito!";
                return resp;
            }
            catch (Exception)
            {
                resp.Success = false;
                resp.Message = "Error al tratar guardar Actor";
                return resp;

            }

        }

        public MyResponse EditarActor(ActorViewModel actorModel)
        {
            MyResponse resp = new MyResponse();
            try
            {
                ActorEntity Actor = new ActorEntity();
                Actor.IdActor = actorModel.IdActor;
                Actor.Nombre = actorModel.Nombre;
                Actor.FechaNacimiento = actorModel.FechaNacimiento;
                Actor.Sexo = actorModel.Sexo;
                Actor.Foto = actorModel.Foto;
                db.Actores.Update(Actor);
                db.SaveChanges();
                resp.Success = true;
                resp.Message = "Datos de Actor Editados con Exito!";
                return resp;
            }
            catch (Exception)
            {
                resp.Success = false;
                resp.Message = "Error al tratar editar Actor";
                return resp;

            }


        }

        public MyResponse EliminarActor(int id)
        {
            MyResponse resp = new MyResponse();
            try
            {
                ActorEntity Actor = new ActorEntity();
                Actor.IdActor = id;
                db.Actores.Remove(Actor);
                db.SaveChanges();
                resp.Success = true;
                resp.Message = "Actor Eliminado con Exito!";
                return resp;
            }
            catch (Exception)
            {
                resp.Success = false;
                resp.Message = "Error al tratar eliminar Actor";
                return resp;
            }


        }


    }
}
