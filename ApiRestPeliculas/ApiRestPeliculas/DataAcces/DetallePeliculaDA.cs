﻿using ApiRestPeliculas.Models.Abstractions;
using ApiRestPeliculas.Models.Entity;
using ApiRestPeliculas.Models.Response;
using ApiRestPeliculas.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiRestPeliculas.DataAcces
{
    public class DetallePeliculaDA : IDetallePeliculaDA
    {
        private MyDBContext db;

        public DetallePeliculaDA(MyDBContext context)
        {
            db = context;
        }

        public IEnumerable<DetallePeliculaViewModel> ListadoDetallePeliculas()
        {
            try
            {
                List<DetallePeliculaViewModel> lst = (from d in db.DetallePeliculas
                                                      select new DetallePeliculaViewModel
                                                      {
                                                          Id = d.Id,
                                                          IdPelicula = d.IdPelicula,
                                                          NombrePelicula = d.NombrePelicula,
                                                          NombreActor = d.NombreActor
                                                      }).ToList();


                return lst;
            }
            catch (Exception)
            {
                List<DetallePeliculaViewModel> lst = null;
                return lst;
            }
        }

        public IEnumerable<DetallePeliculaViewModel> BuscarDetallePelicula(int id)
        {
            try
            {
                List<DetallePeliculaViewModel> lst = (from d in db.DetallePeliculas
                                                      where d.IdPelicula == id
                                                      select new DetallePeliculaViewModel
                                                      {
                                                          Id = d.Id,
                                                          IdPelicula = d.IdPelicula,
                                                          NombrePelicula = d.NombrePelicula,
                                                          NombreActor = d.NombreActor
                                                      }).ToList();


                return lst;
            }
            catch (Exception)
            {
                List<DetallePeliculaViewModel> lst = null;
                return lst;
            }
        }

        public object BuscarDetallePeliculaId(int id)
        {
            MyResponse resp = new MyResponse();
            var detallePelicula = db.DetallePeliculas.Find(id);
            try
            {
                if (detallePelicula == null)
                {
                    resp.Success = false;
                    resp.Message = "Detalle de Pelicula no existe!";
                    resp.Data = detallePelicula;
                    return resp;
                }

                return detallePelicula;
            }
            catch (Exception)
            {
                resp.Success = false;
                resp.Message = "Error al tratar de buscar detalle de pelicula!";
                resp.Data = detallePelicula;
                return resp;
            }


        }

        public MyResponse GuardarDetallePelicula(DetallePeliculaViewModel detallepeliculaModel)
        {
            MyResponse resp = new MyResponse();
            try
            {
                DetallePeliculaEntity detallePelicula = new DetallePeliculaEntity();
                detallePelicula.Id = detallepeliculaModel.Id;
                detallePelicula.IdPelicula = detallepeliculaModel.IdPelicula;
                detallePelicula.NombrePelicula = detallepeliculaModel.NombrePelicula;
                detallePelicula.NombreActor = detallepeliculaModel.NombreActor;
                db.DetallePeliculas.Add(detallePelicula);
                db.SaveChanges();
                resp.Success = true;
                resp.Message = "Detalle de Pelicula Guardada con Exito!";
                return resp;
            }
            catch (Exception)
            {
                resp.Success = false;
                resp.Message = "Error al tratar guardar Detalle de Pelicula";
                return resp;

            }

        }

        public MyResponse EditarDetallePelicula(DetallePeliculaViewModel detalleModel)
        {
            MyResponse resp = new MyResponse();
            try
            {
                DetallePeliculaEntity detallePelicula = new DetallePeliculaEntity();
                detallePelicula.Id = detalleModel.Id;
                detallePelicula.IdPelicula = detalleModel.IdPelicula;
                detallePelicula.NombrePelicula = detalleModel.NombrePelicula;
                detallePelicula.NombreActor = detalleModel.NombreActor;
                db.DetallePeliculas.Update(detallePelicula);
                db.SaveChanges();
                resp.Success = true;
                resp.Message = "Detalle de Pelicula Editada con Exito!";
                return resp;
            }
            catch (Exception)
            {
                resp.Success = false;
                resp.Message = "Error al tratar editar Detalle de Pelicula";
                return resp;

            }


        }

        public MyResponse EliminarDetallePelicula(int id)
        {
            MyResponse resp = new MyResponse();
            try
            {
                DetallePeliculaEntity detallePelicula = new DetallePeliculaEntity();
                detallePelicula.Id = id;
                db.DetallePeliculas.Remove(detallePelicula);
                db.SaveChanges();
                resp.Success = true;
                resp.Message = "Detalle de Pelicula Eliminado con Exito!";
                return resp;
            }
            catch (Exception)
            {
                resp.Success = false;
                resp.Message = "Error al tratar eliminar Detalle de Pelicula";
                return resp;
            }


        }


    }
}
