﻿using ApiRestPeliculas.Models.Abstractions;
using ApiRestPeliculas.Models.Entity;
using ApiRestPeliculas.Models.Response;
using ApiRestPeliculas.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiRestPeliculas.DataAcces
{
    public class PeliculaDA : IPeliculaDA
    {
        private MyDBContext db;

        public PeliculaDA(MyDBContext context)
        {
            db = context;
        }

        public IEnumerable<PeliculaViewModel> ListadoPeliculas()
        {
            try
            {
                List<PeliculaViewModel> lst = (from d in db.Peliculas
                                               select new PeliculaViewModel
                                               {
                                                   IdPelicula = d.IdPelicula,
                                                   Titulo = d.Titulo,
                                                   Genero = d.Genero,
                                                   FechaEstreno = d.FechaEstreno,
                                                   Foto = d.Foto
                                               }).ToList();


                return lst;
            }
            catch (Exception)
            {
                List<PeliculaViewModel> lst = null;
                return lst;
            }
        }

        public object BuscarPelicula(int id)
        {
            MyResponse resp = new MyResponse();
            var Pelicula = db.Peliculas.Find(id);

            try
            {
                if (Pelicula == null)
                {
                    resp.Success = false;
                    resp.Message = "Pelicula no existe!";
                    resp.Data = Pelicula;
                    return resp;
                }

                return Pelicula;
            }
            catch (Exception)
            {
                resp.Success = false;
                resp.Message = "Error al tratar de buscar Pelicula!";
                resp.Data = Pelicula;
                return resp;
            }


        }

        public MyResponse GuardarPelicula(PeliculaViewModel peliculaModel)
        {
            MyResponse resp = new MyResponse();
            try
            {
                PeliculaEntity Pelicula = new PeliculaEntity();
                Pelicula.Titulo = peliculaModel.Titulo;
                Pelicula.Genero = peliculaModel.Genero;
                Pelicula.FechaEstreno = peliculaModel.FechaEstreno;
                Pelicula.Foto = peliculaModel.Foto;
                db.Peliculas.Add(Pelicula);
                db.SaveChanges();
                resp.Success = true;
                resp.Message = "Pelicula Guardada con Exito!";
                return resp;
            }
            catch (Exception)
            {
                resp.Success = false;
                resp.Message = "Error al tratar guardar Pelicula";
                return resp;

            }



        }

        public MyResponse EditarPelicula(PeliculaViewModel model)
        {
            MyResponse resp = new MyResponse();
            try
            {
                PeliculaEntity Pelicula = new PeliculaEntity();
                Pelicula.IdPelicula = model.IdPelicula;
                Pelicula.Titulo = model.Titulo;
                Pelicula.Genero = model.Genero;
                Pelicula.FechaEstreno = model.FechaEstreno;
                Pelicula.Foto = model.Foto;
                db.Peliculas.Update(Pelicula);
                db.SaveChanges();
                resp.Success = true;
                resp.Message = "Datos de Pelicula Editados con Exito!";
                return resp;
            }
            catch (Exception)
            {
                resp.Success = false;
                resp.Message = "Error al tratar editar Pelicula";
                return resp;

            }


        }

        public MyResponse EliminarPelicula(int id)
        {
            MyResponse resp = new MyResponse();
            try
            {
                PeliculaEntity Pelicula = new PeliculaEntity();
                Pelicula.IdPelicula = id;
                db.Peliculas.Remove(Pelicula);
                db.SaveChanges();
                resp.Success = true;
                resp.Message = "Pelicula Eliminada con Exito!";
                return resp;
            }
            catch (Exception)
            {
                resp.Success = false;
                resp.Message = "Error al tratar eliminar Pelicula";
                return resp;
            }


        }


    }
}
