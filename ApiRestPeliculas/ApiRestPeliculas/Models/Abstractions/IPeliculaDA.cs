﻿using ApiRestPeliculas.Models.Response;
using ApiRestPeliculas.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiRestPeliculas.Models.Abstractions
{
    public interface IPeliculaDA
    {
        IEnumerable<PeliculaViewModel> ListadoPeliculas();

        object BuscarPelicula(int id);

        MyResponse GuardarPelicula(PeliculaViewModel peliculaModel);

        MyResponse EditarPelicula(PeliculaViewModel model);

        MyResponse EliminarPelicula(int id);

    }
}
