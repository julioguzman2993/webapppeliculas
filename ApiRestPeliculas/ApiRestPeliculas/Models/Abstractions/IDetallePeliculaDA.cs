﻿using ApiRestPeliculas.Models.Response;
using ApiRestPeliculas.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiRestPeliculas.Models.Abstractions
{
    public interface IDetallePeliculaDA
    {
        IEnumerable<DetallePeliculaViewModel> ListadoDetallePeliculas();

        IEnumerable<DetallePeliculaViewModel> BuscarDetallePelicula(int id);

        object BuscarDetallePeliculaId(int id);

        MyResponse GuardarDetallePelicula(DetallePeliculaViewModel detallepeliculaModel);

        MyResponse EditarDetallePelicula(DetallePeliculaViewModel detalleModel);

        MyResponse EliminarDetallePelicula(int id);


    }
}
