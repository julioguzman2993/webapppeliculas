﻿using ApiRestPeliculas.Models.Response;
using ApiRestPeliculas.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiRestPeliculas.Models.Abstractions
{
    public interface IDetalleActorDA
    {
        IEnumerable<DetalleActorViewModel> ListadoDetallesActores();

        IEnumerable<DetalleActorViewModel> BuscarDetalleActor(int id);

        object BuscarDetalleActorId(int id);

        MyResponse GuardarDetalleActor(DetalleActorViewModel detalleActorModel);

        MyResponse EditarDetalleActor(DetalleActorViewModel detalleActorModel);

        MyResponse EliminarDetalleActor(int id);

    }
}
