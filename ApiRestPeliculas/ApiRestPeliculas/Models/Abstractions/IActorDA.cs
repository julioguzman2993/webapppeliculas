﻿using ApiRestPeliculas.Models.Response;
using ApiRestPeliculas.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiRestPeliculas.Models.Abstractions
{
    public interface IActorDA
    {
        IEnumerable<ActorViewModel> ListadoActores();

        object BuscarActor(int id);

        MyResponse GuardarActor(ActorViewModel actorModel);

        MyResponse EditarActor(ActorViewModel actorModel);

        MyResponse EliminarActor(int id);

    }
}
