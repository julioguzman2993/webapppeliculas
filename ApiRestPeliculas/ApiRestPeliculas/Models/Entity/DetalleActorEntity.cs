﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ApiRestPeliculas.Models.Entity
{
    public class DetalleActorEntity
    {
        [Key]
        [Required]
        public int Id { get; set; }
        [Required]
        public int IdActor { get; set; }
        [Required]
        public string NombreActor { get; set; }
        [Required]
        public string NombrePelicula { get; set; }
    }
}

