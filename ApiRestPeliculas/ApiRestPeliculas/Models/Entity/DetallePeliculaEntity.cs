﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ApiRestPeliculas.Models.Entity
{
    public class DetallePeliculaEntity
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public int IdPelicula { get; set; }
        [Required]
        public string NombrePelicula { get; set; }
        [Required]
        public string NombreActor { get; set; }
    }
}
