﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiRestPeliculas.Models.ViewModels
{
    public class LoginViewModel
    {
        public string Usuario { get; set; }
        public string Clave { get; set; }
    }
}
