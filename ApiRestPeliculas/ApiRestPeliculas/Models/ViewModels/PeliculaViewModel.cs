﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ApiRestPeliculas.Models.ViewModels
{
    public class PeliculaViewModel
    {
        [Required]
        public int IdPelicula { get; set; }
        [Required]
        public string Titulo { get; set; }
        [Required]
        public string Genero { get; set; }
        [Required]
        public DateTime FechaEstreno { get; set; }
        [Required]
        public string Foto { get; set; }
    }
}
